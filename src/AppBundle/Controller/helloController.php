<?php
namespace AppBundle\Controller;
    
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;



//1 solo parametro
//class helloController
//{
//    /**
//    *   @Route("/hello/{name}", name="hello")
//    */
//    public function indexAction($name)
//    {
//    return new Response('<html><body>Hello '.$name.'!</body></html>');
//    }
//}

//2 parametros
//class helloController
//{
//    /**
//   *   @Route("/hello/{firstName}/{lastName}", name="hello")
//    */
//    public function indexAction($firstName, $lastName)
//   {
//    return new Response('<html><body>Hello '.$firstName . $lastName .'!</body></html>');
//    }
//}


//redireccionar
class helloController extends Controller
{
    /**
   *   @Route("/hello")
    */
   //Redireciona al home
    // public function indexAction()
   //{
    //return $this->redirectToRoute('homepage');
   // }
    
    
    //redireciona al home de otra manera
    //public function indexAction()
    //{
    //    return $this->redirectToRoute('homepage', array(),301);
    //}
    
    //te redireciona a la pagina que tu le pongas
    //public function indexAction()
    //{
     //   return $this->redirect('http://symfony.com/doc');
    //}
    
    //Redirecion mediante una clase de redirection repsonse
   // public function indexAction()
    //{
    //    return new RedirectResponse($this->generateurl('homepage'));
   // }
    
    // no funciona falta plantilla y error en el name
    //public function indexAction()
    //{
     //   return $this->render('hello/index.html.twig', array('name'=> $name));
    //}
    
    //no funciona
    //public function indexAction()
    //{
        // retrive the object from database
        
        //$product = ...;
        //if(!$product){
            //throw $this->createNotFoundException('The produc does not exist');   
            //throw new \Exception('Something went wrong!');
        //}
        //return $this->render(...);
    //}
   
    //nose para que funciona
   // public function indexAction($firstName, $lastName, Request $request)
   // {
   //    $page = $request->query->get('page',1);
   // }
    
    
    // public function indexAction(Request $request)
    //{
     //$session = $request->getSession();
         
     //$foobar = $session->get('foobar');
         
     //$filters = $session->get('filters', array());
     //}
}